# -*- coding: utf-8 -*-
"""
SOPRA with prebuilt contigs workflow runner
"""

import logging
import optparse
import os
import tempfile
import shutil
import subprocess

class SOPRA:

    def __init__(self, options, logger):
        self.logger = logger
        self.logger.debug('Creating temp dir')
        self.wd = tempfile.mkdtemp()
        self.scaffolds = options.scaffolds

    def run(self, contigs, mate, c_option, w_option, L_option, insert_size):
        """ """
        fake_mate = os.path.join(self.wd, os.path.basename(mate) + '.fasta') # s_prep_contigAseq_v1.4.6.pl wants a file with extension [Ff][Aa][Ss][Tt][Aa] or [Ff][Aa]
        contigs_sopra = os.path.join(self.wd, 'contigs_sopra.fasta') # s_prep_contigAseq_v1.4.6.pl always writes the contigs to this file
        bowtie_build = os.path.join(self.wd, 'bowtie_build') # arbitrary basename for bowtie-build output files
        mate_sopra = os.path.splitext(fake_mate)[0] + '_sopra.fasta' # s_prep_contigAseq_v1.4.6.pl writes the paired reads to this file
        mysam_mate = os.path.join(self.wd, 'mysam_mate') # arbitrary filename for bowtie output in SAM format
        mysam_mate_parsed = mysam_mate + '_parsed' # s_parse_sam_v1.4.6.pl writes its output to this file
        
        self.logger.debug('Coping fake_mate: ' + fake_mate)
        shutil.copy2(mate, fake_mate)
        
        cmd_step1 = 's_prep_contigAseq_v1.4.6.pl -contig %s -mate %s -a %s' % (contigs, fake_mate, self.wd)
        cmd_step2 = 'bowtie-build %s %s' % (contigs_sopra, bowtie_build)
        cmd_step3 = 'bowtie -v 0 -m 1 -f --sam %s %s > %s' % (bowtie_build, mate_sopra, mysam_mate)
        cmd_step4 = 's_parse_sam_v1.4.6.pl -sam %s -a %s' % (mysam_mate, self.wd)
        cmd_step5 = 's_read_parsed_sam_v1.4.6.pl -parsed %s -d %d -c %d -a %s' % (mysam_mate_parsed, insert_size, c_option, self.wd)
        cmd_step6 = 's_scaf_v1.4.6.pl -w %d -L %d -o %s -a %s' % (w_option, L_option, os.path.join(self.wd, 'orientdistinfo_c%d' % c_option), self.wd)
        
        self.logger.info("running step1: preparation")
        self.logger.debug("cmd step1: %s" % cmd_step1)
        subprocess.check_call(args=cmd_step1, shell=True)
        
        self.logger.info("running step2: bowtie building index")
        self.logger.debug("cmd step2: %s" % cmd_step2)
        subprocess.check_call(args=cmd_step2, shell=True)
        
        self.logger.info("running step3: bowtie alignment")
        self.logger.debug("cmd step3: %s" % cmd_step3)
        subprocess.check_call(args=cmd_step3, shell=True)
        
        self.logger.info("running step4: removing pairs")
        self.logger.debug("cmd step4: %s" % cmd_step4)
        subprocess.check_call(args=cmd_step4, shell=True)
        
        self.logger.info("running step5: read parsed sam")
        self.logger.debug("cmd step5: %s" % cmd_step5)
        subprocess.check_call(args=cmd_step5, shell=True)
        
        self.logger.info("running step6: scaffold assembly")
        self.logger.debug("cmd step6: %s" % cmd_step6)
        subprocess.check_call(args=cmd_step6, shell=True)
        
        scaffolds_file = os.path.join(self.wd, 'scaffolds_h2.2_L%d_w%d.fasta' % (L_option, w_option))
        self.logger.info("Moving result file")
        shutil.move(scaffolds_file, self.scaffolds)

    def __del__(self):
        shutil.rmtree(self.wd)

### END OF CLASS

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']


def __main__():
    """ main function """
    parser = optparse.OptionParser(description='SOPRA with prebuilt contigs')
    parser.add_option('-l', '--logfile', help='log file (default=stderr)')
    parser.add_option('--loglevel', choices=LOG_LEVELS, help='logging level (default: INFO)', default='INFO')
    parser.add_option('--mate', dest='mate', help='paired-end Illumina library, fasta file mandatory')
    parser.add_option('--contigs', dest='contigs', help='contigs fasta file mandatory')
    parser.add_option('--scaffolds', dest='scaffolds', help='scaffolds fasta file mandatory')
    parser.add_option('-d', dest='insert_size', type='int', help='Insert size for the corresponding mate pair library')
    parser.add_option('-c', dest='c_option', type='int', default=5, help='If the number of times a read and its reverse complement appear in the library is equal to or more than this value, the pairing information from that read will be disregarded')
    parser.add_option('-w', dest='w_option', type='int', default=4, help='Minimum number of links between two contigs')
    parser.add_option('-L', dest='L_option', type='int', default=150, help='Minimum length of contigs to be used in scaffold assembly')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    log_level = getattr(logging, options.loglevel)
    kwargs = {'format' : LOG_FORMAT,
              'datefmt' : LOG_DATEFMT,
              'level' : log_level}
    if options.logfile:
        kwargs['filename'] = options.logfile
    logging.basicConfig(**kwargs)
    logger = logging.getLogger('SOPRA scaffold assemby')
    
    S = SOPRA(options, logger)
    S.run(options.contigs, options.mate, options.c_option, options.w_option, options.L_option, options.insert_size)


if __name__ == "__main__":
    __main__()
