# -*- coding: utf-8 -*-
"""
Mugsy wrapper

For draft genomes a single multi-FASTA file containig all contigs should be provided.
"""

import optparse
import os
import shutil
import subprocess
import sys
import tempfile

def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-r', dest='reference', help='reference FASTA file')
    parser.add_option('-c', dest='contigs', help='contigs FASTA file')
    parser.add_option('-p', dest='prefix', default='prefix', help='prefix ')
    parser.add_option('-l', dest='logfile', help='logfile')
    parser.add_option('--ml', dest='mugsylog', help='mugsylog file')
    parser.add_option('--maf', dest='maf', help='ouput MAF alignment file ')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    logfile = options.logfile
    wd = tempfile.mkdtemp()
    try:
        command = "mugsy --directory %s --prefix %s --log %s %s %s" % (wd, options.prefix, options.mugsylog, options.reference, options.contigs)
        print 'Mugsy command to be executed:\n ' + command
        
        log = open(logfile, 'w') if logfile else sys.stdout
        try:
            subprocess.check_call(command, stderr=log, shell=True) # need to redirect stderr because mugsy writes its logging info there
        finally:
            if log != sys.stdout:
                log.close()
        print 'Mugsy executed!'
        
        shutil.move(os.path.join(wd, options.prefix + '.maf'), options.maf)
    finally:
        shutil.rmtree(wd)


if __name__ == "__main__":
    __main__()
