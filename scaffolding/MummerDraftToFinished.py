# -*- coding: utf-8 -*-
"""
MummerDraftToFinished.py -r reference.fasta -d draft.fasta -p testgal -delta testgaldelta.txt -coord testgalcoord.txt -align testgalalig.txt -filters testgalfilt.txt -snps testgalsnps.txt
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-r', dest='reference', help='reference')
    parser.add_option('-d', dest='draft', help='draft')
    parser.add_option('-p', dest='prefix', default='prefix', help='prefix')
    parser.add_option('--delta', dest='delta', help='delta file')
    parser.add_option('--coord', dest='coords', help='coord file')
    parser.add_option('--align', dest='aligns', help='align file')
    parser.add_option('--filters', dest='filters', help='filter file')
    parser.add_option('--snps', dest='snps', help='SNPs file')
    parser.add_option('--log', dest='logfile', help='log file')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    reference = options.reference
    draft = options.draft
    prefix = options.prefix
    delta = options.delta
    coords = options.coords
    aligns = options.aligns
    filters = options.filters
    snps = options.snps
    logfile = options.logfile
    
    command = "nucmer --prefix=%s %s %s" % (prefix, reference, draft)
    print command
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(command, stderr=log, shell=True) # need to redirect stderr because nucmer writes its logging info there
    finally:
        if log != sys.stdout:
            log.close()
    shutil.move(prefix + '.delta', delta)
    
    command = "show-coords -rcl %s" % (delta)
    print command
    with open(coords, 'w') as coordsfile:
        subprocess.check_call(command, stdout=coordsfile, shell=True)
    
    print "Getting names"
    with open(reference) as reffile:
        refname = reffile.next().split()[0].replace('>', '')
    with open(draft) as draftfile:
        draftname = draftfile.next().split()[0].replace('>', '')
    
    command = "show-aligns %s '%s' '%s'" % (delta, refname, draftname)
    print command
    with open(aligns, 'w') as alignsfile:
        subprocess.check_call(command, stdout=alignsfile, shell=True)
    
    command = "delta-filter -q %s" % (delta)
    print command
    with open(filters, 'w') as filtersfile:
        subprocess.check_call(command, stdout=filtersfile, shell=True)
    
    # SNP detection
    command = "show-snps -Clr %s" % (delta)
    print command
    with open(snps, 'w') as snpsfile:
        subprocess.check_call(command, stdout=snpsfile, shell=True)
    # aggiungere una statistica del risultato finale
    # aggiungere un controllo del FASTA in input


if __name__ == "__main__":
    __main__()
