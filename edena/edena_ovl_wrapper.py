# -*- coding: utf-8 -*-
"""
Edena (overlapping)
version 0.2.1 (andrea.pinna@crs4.it)
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Edena (overlapping) input options...'
    parser = optparse.OptionParser()
    parser.add_option('--unpaired_input', dest='unpaired_input', help='')
    parser.add_option('--dr_pair_1', dest='dr_pair_1', help='')
    parser.add_option('--dr_pair_2', dest='dr_pair_2', help='')
    parser.add_option('--rd_pair_1', dest='rd_pair_1', help='')
    parser.add_option('--rd_pair_2', dest='rd_pair_2', help='')
    parser.add_option('--nThreads', dest='nThreads', type='int', help='')
    parser.add_option('--minOlap', dest='minOlap', type='int', help='')
    parser.add_option('--readsTruncation', dest='readsTruncation', type='int', help='')
    parser.add_option('--output', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # build Edena (overlapping) command to be executed
    # unpaired input(s)
    if options.unpaired_input:
        unpaired_inputs = options.unpaired_input.split('+')[0:-1]
        unpaired_input = '-r'
        for item in unpaired_inputs:
            unpaired_input += ' %s' % (item)
    else:
        unpaired_input = ''
    # direct-reverse paired-end files
    if options.dr_pair_1 and options.dr_pair_2:
        dr_pairs_1 = options.dr_pair_1.split('+')[0:-1]
        dr_pairs_2 = options.dr_pair_2.split('+')[0:-1]
        dr_pairs = '-DRpairs'
        for i in xrange(len(dr_pairs_1)):
            dr_pairs += ' %s %s' % (dr_pairs_1[i], dr_pairs_2[i])
    else:
        dr_pairs = ''
     # reverse-direct paired-end files
    if options.rd_pair_1 and options.rd_pair_2:
        rd_pairs_1 = options.rd_pair_1.split('+')[0:-1]
        rd_pairs_2 = options.rd_pair_2.split('+')[0:-1]
        rd_pairs = '-RDpairs'
        for i in xrange(len(rd_pairs_1)):
            rd_pairs += ' %s %s' % (rd_pairs_1[i], rd_pairs_2[i])
    else:
        rd_pairs = ''
    # nThreads
    if options.nThreads is not None:
        nThreads = '-nThreads %d' % (options.nThreads)
    else:
        nThreads = ''
    # minimum overlap
    if options.minOlap is not None:
        minOlap = '-M %d' % (options.minOlap)
    else:
        minOlap = ''
    # 3' end reads truncation
    if options.readsTruncation is not None:
        readsTruncation = '-t %d' % (options.readsTruncation)
    else:
        readsTruncation = ''
    # output file(s)
    output = options.output
    logfile = options.logfile
    
    # Build Edena (overlapping) command
    cmd = 'edena %s %s %s %s %s %s -p galaxy_output' % (unpaired_input, dr_pairs, rd_pairs, nThreads, minOlap, readsTruncation)
    print '\nEdena (overlapping) command to be executed: \n %s' % ( cmd )
    
    # Execution of Edena
    print 'Executing Edena (overlapping)...'
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because edena writes some logging info there (e.g. "Computing overlaps >=30...")
    finally:
        if log != sys.stdout:
            log.close()
    print 'Edena (overlapping) executed!'
    
    shutil.move( "galaxy_output.ovl", output)


if __name__ == "__main__":
    __main__()
