# -*- coding: utf-8 -*-
"""
Edena (assembling)
version 0.2.1 (andrea.pinna@crs4.it)
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Edena (assembling) input options...'
    parser = optparse.OptionParser(description='Edena assembly')
    parser.add_option('--ovl_input', dest='ovl_input', help='')
    parser.add_option('--overlapCutoff', dest='overlapCutoff', type='int', help='')
    parser.add_option('--cc', action="store_true", dest='cc', help='')
    parser.add_option('--discardNonUsable', action="store_true", dest='discardNonUsable', help='')
    parser.add_option('--minContigSize', dest='minContigSize', type='int', help='')
    parser.add_option('--minCoverage', dest='minCoverage', type='float', help='')
    parser.add_option('--trim', dest='trim', type='int', help='')
    parser.add_option('--peHorizon', dest='peHorizon', type='int', help='')
    parser.add_option('--covStats', dest='covStats', help='')
    parser.add_option('--out_contigs_cov', dest='out_contigs_cov', help='')
    parser.add_option('--out_contigs_fasta', dest='out_contigs_fasta', help='')
    parser.add_option('--out_contigs_lay', dest='out_contigs_lay', help='')
    parser.add_option('--out_log_txt', dest='out_log_txt', help='')
    parser.add_option('--out_nodesInfo', dest='out_nodesInfo', help='')
    parser.add_option('--out_nodesPosition', dest='out_nodesPosition', help='')
    parser.add_option('--logfile', dest='logfile', help='logfile')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # build Edena (assembling) command to be executed
    ovl_input = '-e %s' % (options.ovl_input)
    if options.overlapCutoff is not None:
        overlapCutoff = '-m %d' % (options.overlapCutoff)
    else:
        overlapCutoff = ''
    if options.cc:
        cc = '-cc yes'
    else:
        cc = '-cc no'
    if options.discardNonUsable:
        discardNonUsable = '-discardNonUsable yes'
    else:
        discardNonUsable = '-discardNonUsable no'
    if options.minContigSize is not None:
        minContigSize = '-c %d' % (options.minContigSize)
    else:
        minContigSize = ''
    if options.minCoverage is not None:
        minCoverage = '-minCoverage %s' % (options.minCoverage)
    else:
        minCoverage = ''
    if options.trim is not None:
        trim = '-trim %d' % (options.trim)
    else:
        trim = ''
    if options.peHorizon is not None:
        peHorizon = '-peHorizon %d' % (options.peHorizon)
    else:
        peHorizon = ''
    covStats = options.covStats
    out_contigs_cov = options.out_contigs_cov
    out_contigs_fasta = options.out_contigs_fasta
    out_contigs_lay = options.out_contigs_lay
    out_log_txt = options.out_log_txt
    out_nodesInfo = options.out_nodesInfo
    out_nodesPosition = options.out_nodesPosition
    logfile = options.logfile
    
    # Build Edena (assembling) command
    cmd1 = '%s %s %s %s %s %s %s %s' % (ovl_input, overlapCutoff, cc, discardNonUsable, minContigSize, minCoverage, trim, peHorizon)
    cmd2 = 'edena %s' % ( cmd1 )
    print '\nEdena (assembling) command to be executed: \n %s' % ( cmd2 )
    
    # Execution of Edena
    print 'Executing Edena (assembling)...'
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(cmd2, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because edena writes some logging info there (e.g. "Condensing overlaps graph...")
    finally:
        if log != sys.stdout:
            log.close()
    print 'Edena (assembling) executed!'

    shutil.move("covStats", covStats)
    shutil.move("out_contigs.cov", out_contigs_cov)
    shutil.move("out_contigs.fasta", out_contigs_fasta)
    shutil.move("out_contigs.lay", out_contigs_lay)
    shutil.move("out_log.txt", out_log_txt)
    shutil.move("out_nodesInfo", out_nodesInfo)
    shutil.move("out_nodesPosition", out_nodesPosition)


if __name__ == "__main__":
    __main__()
