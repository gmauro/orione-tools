# -*- coding: utf-8 -*-
"""
MUSCLE wrapper
"""

import optparse
import subprocess
import sys

def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-i', dest='infile', help='input file in multi-FASTA format')
    parser.add_option('-o', dest='outfile', help='output file name')
    parser.add_option('-m', dest='maxiters', type='int', help='max iteration')
    parser.add_option('-d', action='store_true', dest='diags', help='diagonal')
    parser.add_option('-l', dest='logfile', help='log file name')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    maxiters = "-maxiters %d" % options.maxiters if options.maxiters is not None else ''
    diags = '-diags' if options.diags else ''
    logfile = options.logfile
    
    command = "muscle -in %s -out %s %s %s" % (options.infile, options.outfile, maxiters, diags)
    print 'MUSCLE command to be executed: \n %s' % ( command )
    
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(command, stderr=log, shell=True) # needs to redirect stderr because MUSCLE writes logging info there
    finally:
        if log != sys.stdout:
            log.close()


if __name__ == "__main__":
    __main__()
