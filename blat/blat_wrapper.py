# -*- coding: utf-8 -*-
"""
BLAT
version 0.2.1 (andrea.pinna@crs4.it)
"""

import optparse
import subprocess
import sys

def check_password(password_file, input_password):
    with open(password_file, 'r') as f:
        lines = f.readlines()
    
    continue_reading = True
    i = 0
    is_correct = False
    while continue_reading and i < len(lines):
        current_password = lines[i].split('\t')[0]
        current_user = lines[i].split('\t')[1][0:-1]
        #print 'Current password: %s' % current_password
        #print 'Current user: %s' % current_user
        if current_password == input_password:
            continue_reading = False
            is_correct = True
            #print 'User: %s' % current_user
        i = i + 1
        #print 'Current item: %s\n' % i
    
    if is_correct:
        print 'User \'%s\' allowed to BLAT!\n' % current_user
    else:
        print 'Password does not match!\n'
    return is_correct


def __main__():
    # load arguments
    print 'Parsing input options...'
    parser = optparse.OptionParser()
    parser.add_option('--database', dest='database', help='')
    parser.add_option('--query', dest='query', help='')
    parser.add_option('--t', dest='databaseType', choices=['dna', 'prot', 'dnax'], help='')
    parser.add_option('--q', dest='queryType', choices=['dna', 'rna', 'prot', 'dnax', 'rnax'], help='')
    parser.add_option('--ooc', dest='ooc', help='')
    parser.add_option('--tileSize', dest='tileSize', type='int', help='')
    parser.add_option('--stepSize', dest='stepSize', type='int', help='')
    parser.add_option('--oneOff', action='store_true', dest='oneOff', help='')
    parser.add_option('--minMatch', dest='minMatch', type='int', help='')
    parser.add_option('--minScore', dest='minScore', type='int', help='')
    parser.add_option('--minIdentity', dest='minIdentity', type='int', help='')
    parser.add_option('--maxGap', dest='maxGap', type='int', help='')
    parser.add_option('--noHead', action='store_true', dest='noHead', help='')
    parser.add_option('--makeOoc', dest='makeOoc', help='')
    parser.add_option('--repMatch', dest='repMatch', type='int', help='')
    parser.add_option('--maskType', dest='maskType', choices=['lower', 'upper', 'out', 'file.out'], help='')
    parser.add_option('--qMaskType', dest='qMaskType', choices=['lower', 'upper', 'out', 'file.out'], help='')
    parser.add_option('--repeatsType', dest='repeatsType', choices=['lower', 'upper', 'out', 'file.out'], help='')
    parser.add_option('--minRepDivergence', dest='minRepDivergence', type='int', help='')
    parser.add_option('--dots', dest='dots', type='int', help='')
    parser.add_option('--trimT', action='store_true', dest='trimT', help='')
    parser.add_option('--noTrimA', action='store_true', dest='noTrimA', help='')
    parser.add_option('--trimHardA', action='store_true', dest='trimHardA', help='')
    parser.add_option('--fastMap', action='store_true', dest='fastMap', help='')
    parser.add_option('--outType', dest='outType', choices=['psl', 'pslx', 'axt', 'maf', 'sim4', 'wublast', 'blast', 'blast8', 'blast9'], help='')
    parser.add_option('--fine', action='store_true', dest='fine', help='')
    parser.add_option('--maxIntron', dest='maxIntron', type='int', help='')
    parser.add_option('--extendThroughN', action='store_true', dest='extendThroughN', help='')
    parser.add_option('--blat_password', dest='blat_password', help='')
    parser.add_option('--output', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # check BLAT password
    is_correct = check_password('/u/galaxy/galaxy_passwords.tsv', options.blat_password)
    if is_correct:
        print 'Correct BLAT password!'
    else:
        sys.exit('Wrong BLAT password!!!\n')
    
    # build BLAT command to be executed
    # database file
    database = options.database
    # query file
    query = options.query
    # database type
    t = "-t=%s" % (options.databaseType) if options.databaseType else ''
    # query type
    q = "-q=%s" % (options.queryType) if options.queryType else ''
    # tile size
    tileSize = "-tileSize=%d" % (options.tileSize) if options.tileSize is not None and options.tileSize >= 1 else ''
    # step size
    stepSize = "-stepSize=%d" % (options.stepSize) if options.stepSize is not None and options.stepSize >= 1 else ''
    # one off
    oneOff = '-oneOff=1' if options.oneOff else ''
    # min match
    minMatch = "-minMatch=%d" % (options.minMatch) if options.minMatch is not None and options.minMatch >= 0 else ''
    # min score
    minScore = "-minScore=%d" % (options.minScore) if options.minScore is not None and options.minScore >= 1 else ''
    # min identity
    minIdentity = "-minIdentity=%d" % (options.minIdentity) if options.minIdentity is not None and options.minIdentity >= 1 else ''
    # max gap
    maxGap = "-maxGap=%d" % (options.maxGap) if options.maxGap is not None and options.maxGap >= 1 else ''
    # no head
    noHead = '-noHead' if options.noHead else ''
    # rep match
    repMatch = "-repMatch=%d" % (options.repMatch) if options.repMatch is not None and options.repMatch >= 1 else ''
    # mask type
    maskType = "-mask=%s" % (options.maskType) if options.maskType else ''
    # qmask type
    qMaskType = "-qMask=%s" % (options.qMaskType) if options.qMaskType else ''
    # repeats type
    repeatsType = "-repeats=%s" % (options.repeatsType) if options.repeatsType else ''
    # min rep divergence
    minRepDivergence = "-minRepDivergence=%d" % (options.minRepDivergence) if options.minRepDivergence is not None and options.minRepDivergence >= 1 else ''
    # dots
    dots = "-dots=%d" % (options.dots) if options.dots is not None and options.dots >= 1 else ''
    # trim t
    trimT = '-trimT' if options.trimT else ''
    # no trim a
    noTrimA = '-noTrimA' if options.noTrimA else ''
    # trim hard a
    trimHardA = '-trimHardA' if options.trimHardA else ''
    # fast map
    fastMap = '-fastMap' if options.fastMap else ''
    # output type
    outType = "-out=%s" % (options.outType) if options.outType else ''
    # fine
    fine = '-fine' if options.fine else ''
    # max intron
    maxIntron = "-maxIntron=%d" % (options.maxIntron) if options.maxIntron is not None and options.maxIntron >= 1 else ''
    # extend through n
    extendThroughN = '-extendThroughN' if options.extendThroughN else ''
    # output file
    output = options.output
    logfile = options.logfile
    
    # Build BLAT command
    cmd = ' '.join(['blat', database, query, t, q, tileSize, stepSize, oneOff, minMatch, minScore, minIdentity, maxGap, noHead, repMatch, maskType, qMaskType, repeatsType, minRepDivergence, dots, trimT, noTrimA, trimHardA, fastMap, outType, fine, maxIntron, extendThroughN, output])
    print '\nBLAT command to be executed:\n ' + cmd
    
    # Execution of BLAT
    print 'Executing BLAT...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'BLAT executed!'


if __name__ == "__main__":
    __main__()
