# -*- coding: utf-8 -*-
"""

"""

import optparse
import os
import re

class Consensus:

    def __init__(self, draft, reference, summary):
        self.infilename = draft
        self.referencename = reference
        self.outfilename = summary
        self.gaps = re.compile('n+')
        self.contig = re.compile('[acgt]+')
        self.genomesize = self._getSize(reference)
        #self.statistics = statistics

    def _getSize(self, reference):
        """ """
        with open(reference) as fasta:
            fasta.next() # skip header
            tmp = ""
            try:
                while True:
                    row = fasta.next().strip()
                    tmp += row
            except StopIteration:
                return len(tmp)

    def analyze(self):
        """ """
        self.fasta = ""
        with open(self.infilename, 'r') as infile:
            infile.next() # skip header
            try:
                while True:
                    row = infile.next().strip().lower()
                    self.fasta += row
            except StopIteration:
                self.fasta.replace("\n", "")
        
        ns = self.fasta.count("n")
        nsperc =  float(ns)/ float(len(self.fasta)) * 100
        gapsiterator   = self.gaps.findall(self.fasta)
        contigiterator = self.contig.findall(self.fasta)
        self.gapslength = []
        self.contigslength = []
        for gap in gapsiterator:
            self.gapslength.append(len(gap))
        for contig in contigiterator:
            self.contigslength.append(len(contig))#; print contig
        with open(self.outfilename, 'w') as out:
            out.write("# Draft Evaluator v1.0 on file %s\n" % os.path.basename(self.infilename) )
            out.write("Reference file:\t%s\n" % self.referencename )
            out.write("Reference length:\t%s\n" % str(self.genomesize) )
            out.write("Consensus full length:\t%s\n" % str(len(self.fasta)) )
            out.write("Consensus assembled length:\t%s\n" % str(self._basesAssembled(self.contigslength)) )
            out.write("%% sequenced:\t%s\n" % str(100 - round((nsperc), 1)) )
            out.write("%% gaps:\t%s\n" % str(round((nsperc), 1)) )
            out.write("Num contigs:\t%s\n" % str(len(contigiterator)) )
            out.write("Average contig length:\t%s\n" % str(self._calcAverage(self.contigslength)) )
            out.write("Median contig length:\t%s\n" % str(self._calcMedian(self.contigslength)) )
            out.write("Maximum contig length:\t%s\n" % str(max(self.contigslength)) )
            out.write("Num contigs > 200 bp:\t%s (%s %%)\n" % (str(self._calcOver200(self.contigslength)), str(round((float(self._calcOver200(self.contigslength)) / float(len(contigiterator)) *100), 1)) ) )
            out.write("Num contigs > 2,000 bp:\t%s (%s %%)\n" % (str(self._calcOver2000(self.contigslength)), str(round((float(self._calcOver2000(self.contigslength)) / float(len(contigiterator)) *100), 1)) ) )
            out.write("Num gaps:\t%s\n" % str(len(gapsiterator)) )
            out.write("Average gap length:\t%s\n" % str(self._calcAverage(self.gapslength)) )
            out.write("Median gap length:\t%s\n" % str(self._calcMedian(self.gapslength)) )
            out.write("Maximum gap length:\t%s\n" % str(max(self.gapslength)) )
            out.write("Num gaps < 5 bp:\t%s (%s %%)\n" % (str(self._calcGaps5(self.gapslength)), str(round((float(self._calcGaps5(self.gapslength)) / float(len(gapsiterator)) *100), 1)) ) )
            out.write("N50:\t%s\n" % str(self._calcN50(self.contigslength)) )
            out.write("NG50:\t%s\n" % str(self._calcNG50(self.contigslength)) )
        #if self.statistics == "T":
        #    self._setStatistics()

    def _setStatistics(self):
        """ """
        tmpgaps = []
        for element in self.gapslength:
            tmpgaps.append(str(element))
        with open(self.infilename + '.gap_hist', 'w') as out:
            out.write("\t".join(tmpgaps))
        
        tmpcont = []
        for element in self.contigslength:
            tmpcont.append(str(element))
        with open(self.infilename + '.cont_hist', 'w') as out:
            out.write("\t".join(tmpcont))

    def _calcAverage(self, values):
        """ """
        if len(values) == 0:
            return "-"
        total = 0
        for element in values:
            total += element
        average = total/float(len(values))
        return int(round((average), 1))

    def _calcMedian(self, values):
        """ """
        if len(values) == 0:
            return "-"
        theValues = sorted(values)
        if len(theValues) % 2 == 1:
            return int(theValues[(len(theValues)+1)/2-1])
        else:
            lower = theValues[len(theValues)/2-1]
            upper = theValues[len(theValues)/2]
            return int((float(lower + upper)) / 2)

    def _calcN50(self, values):
        """ """
        theValues = sorted(values)
        theValues.reverse()
        assembled = 0
        for value in values:
            assembled += value
        index = 0
        tmp50 = 0
        while tmp50 < float(assembled)/2:
            tmp50 += theValues[index]
            index += 1
        N50 =  theValues[index]
        return N50

    def _calcNG50(self, values):
        """ """
        theValues = sorted(values)
        theValues.reverse()
        index = 0
        tmpG50 = 0
        try:
            while tmpG50 < float(self.genomesize)/2:
                tmpG50 += theValues[index]
                index += 1
            NG50 =  theValues[index]
        except:
            NG50 = 0
        return NG50

    def _basesAssembled(self, values):
        """ """
        total = 0
        for element in values:
            total += element
        return total

    def _calcOver200(self, values):
        """ """
        total = 0
        for element in values:
            if element >= 200:
                total += 1
        return total

    def _calcOver2000(self, values):
        """ """
        total = 0
        for element in values:
            if element >= 2000:
                total += 1
        return total

    def _calcGaps5(self, values):
        """ """
        total = 0
        for element in values:
            if element <= 5:
                total += 1
        return total


def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-d', dest='draft', help='draft genome')
    parser.add_option('-r', dest='reference', help='reference genome')
    parser.add_option('-o', dest='summary', help='report file ')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    S = Consensus(options.draft, options.reference, options.summary)
    S.analyze()


if __name__ == "__main__":
    __main__()
