# -*- coding: utf-8 -*-
"""

"""

import optparse
import os

class FastaStream:

    def __init__(self, multifastafile):
        """
        Generate a stream of 'FASTA strings' from an io stream.
        """
        self.infile = open(multifastafile)
        self.buffer = []

    def __del__(self) :
        self.infile.close()

    def __iter__(self):
        return self

    def next(self):
        while 1:
            try:
                line = self.infile.next()
                if line.startswith('>') and self.buffer:
                    fasta = "".join(self.buffer)
                    self.buffer = [line]
                    return fasta
                else:
                    self.buffer.append(line)
            except StopIteration:
                if self.buffer:
                    fasta = "".join(self.buffer)
                    self.buffer = []
                    return fasta
                else:
                    raise StopIteration


class Consensus:

    def __init__(self, infilename, genomesize, summary):
        self.infilename = infilename.strip()
        self.outfilename = summary
        self.genomesize = genomesize * 1000000
        self.contigs = []

    def analyze(self):
        """ """
        self._acquire()
        self._counts()

    def _acquire(self):
        """ """
        fastaStream = FastaStream(self.infilename)
        for fasta in fastaStream:
            seq =  "".join(fasta.split("\n")[1:])
            self.contigs.append(len(seq))

    def _counts(self):
        """ """
        with open(self.outfilename, 'w') as out:
            out.write("# Contigs Evaluator v1.0 on file %s \n" % os.path.basename(self.infilename) )
            out.write("Estimated genome size:\t%s\n" %  str(int(self.genomesize))  )
            out.write("Assembled genome size:\t%s\n" %  str(self._basesAssembled(self.contigs)) )
            out.write("Num contigs:\t%s\n" % str(len(self.contigs)) )
            out.write("Average contig length:\t%s\n" % str(self._calcAverage(self.contigs)) )
            out.write("Median contig length:\t%s\n" %  str(self._calcMedian(self.contigs))  )
            out.write("Maximum contig length:\t%s bp\n" % str(max(self.contigs)) )
            out.write("Num contigs > 200 bp:\t%s (%s %%)\n" % (str(self._calcOver200(self.contigs)), str(round((float(self._calcOver200(self.contigs)) / float(len(self.contigs)) *100), 1)) ) )
            out.write("Num contigs > 2,000 bp:\t%s (%s %%)\n" % (str(self._calcOver2000(self.contigs)), str(round((float(self._calcOver2000(self.contigs)) / float(len(self.contigs)) *100), 1)) ) )
            out.write("N50:\t%s\n" % str(self._calcN50(self.contigs)) )
            out.write("NG50:\t%s\n" % str(self._calcNG50(self.contigs)) )

    def _calcAverage(self, values):
        """ """
        if len(values) == 0:
            return "-"
        total = 0
        for element in values:
            total += element
        average = total / float(len(values))
        return int(round((average), 1))

    def _calcMedian(self, values):
        """ """
        if len(values) == 0:
            return "-"
        theValues = sorted(values)
        if len(theValues) % 2 == 1:
            return int(theValues[(len(theValues)+1)/2 - 1])
        else:
            lower = theValues[len(theValues)/2 - 1]
            upper = theValues[len(theValues) / 2]
            return int((float(lower + upper)) / 2)

    def _calcN50(self, values):
        """ """
        theValues = sorted(values)
        theValues.reverse()
        assembled = 0
        for value in values:
            assembled += value
        index = 0
        tmp50 = 0
        while tmp50 < float(assembled)/2:
            tmp50 += theValues[index]
            index += 1
        N50 =  theValues[index]
        return N50

    def _calcNG50(self, values):
        """ """
        theValues = sorted(values)
        theValues.reverse()
        index = 0
        tmpG50 = 0
        try:
            while tmpG50 < self.genomesize / 2:
                tmpG50 += theValues[index]
                index += 1
            NG50 =  theValues[index]
        except:
            NG50 = 0
        return NG50

    def _basesAssembled(self, values):
        """ """
        total = 0
        for element in values:
            total += element
        return total

    def _calcOver200(self, values):
        """ """
        total = 0
        for element in values:
            if element >= 200:
                total += 1
        return total

    def _calcOver2000(self, values):
        """ """
        total = 0
        for element in values:
            if element >= 2000:
                total += 1
        return total

    def _calcGaps5(self, values):
        """ """
        total = 0
        for element in values:
            if element <= 5:
                total += 1
        return total


def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-f', dest='fasta', help='Consensus File Mandatory ')
    parser.add_option('-g', dest='genomesize', type='float', default=3, help='expected genome size ')
    parser.add_option('-o', dest='summary', help='report file ')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    S = Consensus(options.fasta, options.genomesize, options.summary)
    S.analyze()


if __name__ == "__main__":
    __main__()
