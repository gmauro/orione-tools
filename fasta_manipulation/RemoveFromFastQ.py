# -*- coding: utf-8 -*-
import optparse

class FastqRemover:

    def __init__(self, fastqF, fastqR, fileList, listType, outfastqF, outfastqR, outunmate, outdiscarded, subsequence):
        self.fastqF = fastqF
        self.fastqR = fastqR
        self.outunmate = outunmate
        self.outdiscarded = outdiscarded
        self.subsequence = subsequence
        self.fileList = fileList
        self.listType = listType # id ,position, sequence
        self.toremove = []
        if not outfastqF:
            self.fastqFout = fastqF + '.cleaned'
        else: self.fastqFout = outfastqF
        if not outfastqR:
            self.fastqRout = fastqR + '.cleaned'
        else: self.fastqRout = outfastqR
        self.outL = []
        self.outR = []
        self.outD = []
        self.outU = []

    def remove(self):
        """ """
        self._acquireList()
        if self.listType == "position":
            self._removeByPosition()
        elif self.listType == "id":
            self._removeById()
        else:
            self._removeBySequence()

    def _removeByPosition(self):
        """ """
        index = 1
        fastqLfile = open(self.fastqF)
        fastqRfile = open(self.fastqR)
        try:
            while True:
                headL = fastqLfile.next().strip()
                headR = fastqRfile.next().strip()
                sequenceL = fastqLfile.next().strip()
                sequenceR = fastqRfile.next().strip()
                commentL = fastqLfile.next().strip()
                commentR = fastqRfile.next().strip()
                qualL = fastqLfile.next().strip()
                qualR = fastqRfile.next().strip()
                if index not in self.toremove:
                    self._setOutMate(headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR)
                else:
                    self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                    self._setOutDiscarded(headR, sequenceR, commentR, qualR)
                index += 1
        except StopIteration:
            self._writeDown()
        finally:
            fastqLfile.close()
            fastqRfile.close()

    def _removeById(self):
        """ """
        fastqLfile = open(self.fastqF)
        fastqRfile = open(self.fastqR)
        try:
            while True:
                headL = fastqLfile.next().strip()
                headR = fastqRfile.next().strip()
                sequenceL = fastqLfile.next().strip()
                sequenceR = fastqRfile.next().strip()
                commentL = fastqLfile.next().strip()
                commentR = fastqRfile.next().strip()
                qualL = fastqLfile.next().strip()
                qualR = fastqRfile.next().strip()
                
                #qui il filtro
                control = False
                for element in self.toremove:
                    if element in headL or element in headR: # sostituire con espressione regolare
                        control = True
                        self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                        self._setOutDiscarded(headR, sequenceR, commentR, qualR)
                        break
                if control == False:
                    self._setOutMate(headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR)
        except StopIteration:
            self._writeDown()
        finally:
            fastqLfile.close()
            fastqRfile.close()

    def _removeBySequence(self):
        """ """
        fastqLfile = open(self.fastqF)
        fastqRfile = open(self.fastqR)
        try:
            while True:
                headL = fastqLfile.next().strip()
                headR = fastqRfile.next().strip()
                sequenceL = fastqLfile.next().strip()
                sequenceR = fastqRfile.next().strip()
                commentL = fastqLfile.next().strip()
                commentR = fastqRfile.next().strip()
                qualL = fastqLfile.next().strip()
                qualR = fastqRfile.next().strip()
                if self.subsequence:
                    subseq = self._checkSubSequence(headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR)
                    if subseq:
                        pass
                    else: continue
                
                if sequenceL not in self.toremove and sequenceR not in self.toremove:
                    self._setOutMate(headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR)
                elif sequenceL not in self.toremove and sequenceR in self.toremove:
                    self._setOutUnmate(headL, sequenceL, commentL, qualL)
                    self._setOutDiscarded(headR, sequenceR, commentR, qualR)
                elif sequenceL in self.toremove and sequenceR not in self.toremove:
                    self._setOutUnmate(headR, sequenceR, commentR, qualR)
                    self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                else:
                    self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                    self._setOutDiscarded(headR, sequenceR, commentR, qualR)
        except StopIteration:
            self._writeDown()
            self._writeDownD()
            self._writeDownU()
        finally:
            fastqLfile.close()
            fastqRfile.close()

    def _checkSubSequence(self, headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR):
        """ """
        for seq in self.toremove:
            if seq in sequenceL and seq in sequenceR:
                self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                self._setOutDiscarded(headR, sequenceR, commentR, qualR)
                return False
            elif seq in sequenceL and seq not in sequenceR:
                self._setOutUnmate(headR, sequenceR, commentR, qualR)
                self._setOutDiscarded(headL, sequenceL, commentL, qualL)
                return False
            elif seq not in sequenceL and seq in sequenceR:
                self._setOutUnmate(headL, sequenceL, commentL, qualL)
                self._setOutDiscarded(headR, sequenceR, commentR, qualR)
                return False
            else:
                pass
        return True

    def _acquireList(self):
        """ """
        with open(self.fileList) as filelist:
            for line in filelist.readlines():
                self.toremove.append(line.strip())

    def _setOutMate(self, headL, sequenceL, commentL, qualL, headR, sequenceR, commentR, qualR):
        """ """
        self.outL.append(headL + "\n" + sequenceL + "\n+" + commentL + "\n" + qualL + "\n")
        self.outR.append(headR + "\n" + sequenceR + "\n+" + commentR + "\n" + qualR + "\n")
        if len(self.outL) >= 400000:
            self._writeDown()
            self.outL = []
            self.outR = []

    def _writeDown(self):
        """ """
        with open(self.fastqFout, 'a') as out:
            out.write("".join(self.outL))
        with open(self.fastqRout, 'a') as out:
            out.write("".join(self.outR))

    def _setOutUnmate(self, headL, sequenceL, commentL, qualL):
        """ """
        self.outU.append(headL + "\n" + sequenceL + "\n+" + commentL + "\n" + qualL + "\n")
        if len(self.outU) >= 400000:
            self._writeDownU()
            self.outU = []

    def _setOutDiscarded(self, headL, sequenceL, commentL, qualL):
        """ """
        self.outD.append(headL + "\n" + sequenceL + "\n+" + commentL + "\n" + qualL + "\n")
        if len(self.outD) >= 400000:
            self._writeDownD()
            self.outD = []

    def _writeDownU(self):
        """ """
        with open(self.outunmate, 'a') as out:
            out.write("".join(self.outU))

    def _writeDownD(self):
        """ """
        with open(self.outdiscarded, 'a') as out:
            out.write("".join(self.outD))


def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('--ql', dest='fastqF', help='FASTQ file 1')
    parser.add_option('--qr', dest='fastqR', help='FASTQ file 2')
    parser.add_option('--fl', dest='fileList', help='fileList to remove')
    parser.add_option('--lt', dest='listType', choices=['id', 'position', 'sequences'], help='listtype')
    parser.add_option('--of', dest='outfastqF', help='output FASTQ file 1 cleaned')
    parser.add_option('--or', dest='outfastqR', help='output FASTQ file 1 cleaned')
    parser.add_option('--ou', dest='outunmate', help='output unmate')
    parser.add_option('--od', dest='outdiscarded', help='output discarded')
    parser.add_option('--ss', action="store_true", dest='subsequence', help='sub sequence search')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    F = FastqRemover(options.fastqF, options.fastqR, options.fileList, options.listType, options.outfastqF, options.outfastqR, options.outunmate, options.outdiscarded, options.subsequence)
    F.remove()


if __name__ == "__main__":
    __main__()
