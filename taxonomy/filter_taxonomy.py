# -*- coding: utf-8 -*-
"""
Filter a taxonomy file produced by *Fetch taxonomy* or *Find lowest diagnostic rank* tools according to the relative abundance
"""

import optparse

def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('-i', dest='infilename', help='Input file')
    parser.add_option('-o', dest='outfilename', help='Output file')
    parser.add_option('-t', dest='threshold', type='float', help='Threshold as percentage')
    parser.add_option('-l', dest='logfilename', help='Log file')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    infilename = options.infilename
    outfilename = options.outfilename
    threshold = options.threshold
    abundance_dict = {}
    total = 0
    with open(infilename) as infile:
        for row in infile:
            leafId = row.strip().split('\t')[-1]
            abundance_dict.setdefault(leafId, 0)
            abundance_dict[leafId] += 1
            total += 1
    lowerLimit = float(total) / 100 * threshold
    toRetain = set()
    with open(options.logfilename, 'w') as logfile:
        logfile.write("# Total rows: %d\n" % total)
        logfile.write('# Threshold: ' + str(lowerLimit) + '\n')
        logfile.write('# Taxon ID\tNumber of reads\tRetained\n')
        for leafId, abundance in abundance_dict.iteritems():
            retained = abundance > lowerLimit
            logfile.write(leafId + '\t' + str(abundance) + '\t' + str(retained) + '\n')
            if retained:
                toRetain.add(leafId)
    with open(infilename) as infile:
        with open(outfilename, 'w') as outfile:
            for row in infile:
                leafId = row.strip().split('\t')[-1]
                if leafId in toRetain:
                    outfile.write(row)


if __name__ == "__main__":
    __main__()
