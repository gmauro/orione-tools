# -*- coding: utf-8 -*-
"""
Retrieve from NCBI all protein sequences of the bacterial genus and species present in a MetaPhlAn output file whose abundance is over the specified percentage
"""

from ftplib import FTP
import logging
import optparse
import re

class MetaphlanOut:

    def __init__(self, metaphlanoutfile, abundance):
        self.metaphlanoutfile = metaphlanoutfile
        self.abundance = float(abundance)
        self.genuses = []
        self.species = []
        with open(self.metaphlanoutfile) as metaphlanout:
            for line in metaphlanout.readlines():
                if "s__" in line:
                    taxonomy = line.split("\t")[0]
                    abundance = line.split("\t")[1].strip()
                    if abundance >= self.abundance:
                        genus = taxonomy.split("g__")[1].split("|")[0].strip()
                        specie = taxonomy.split("s__")[1].split("|")[0].strip()
                        if genus not in self.genuses:
                            self.genuses.append(genus)
                        if specie not in self.species:
                            self.species.append(specie)


class GetData:

    def __init__(self, options, logger):
        self.metaphlanfile = options.metaphlanfile
        self.abundance = options.abundance
        self.faagfile = options.faagfile
        self.faasfile = options.faasfile
        self.logfile = options.logfile
        self.logger = logger
        self.ftpurl = 'ftp.ncbi.nlm.nih.gov'
        self.folder = '/genomes/Bacteria/'

    def getData(self):
        """ """
        self._getTargetNames()
        self._getStrainNames()
        self._getFiles(self.toretrieveg, self.faagfile)
        self._getFiles(self.toretrieves, self.faasfile)

    def _getFiles(self, strainNames, filename):
        """ """
        ftp = FTP(self.ftpurl)
        ftp.login()
        strainNum = 0
        strainTot = len(strainNames)
        print "STRNAMES", strainTot

        self.logger.info('--> DOWNLOADING FILES FROM NCBI...')
        with open(filename, 'w') as outFile:
            for strainName in strainNames:
                strainNum += 1
                newDir = self.folder + strainName
                ftp.cwd(newDir)
                directoryFiles = []
                ftp.retrlines('NLST', directoryFiles.append)
                for fileName in directoryFiles:
                    try:
                        if '.faa' in fileName:
                            self.logger.info('DOWNLOADING [' + str(strainNum) + '/' + str(strainTot) + '] '+ strainName + ' ' + fileName)
                            ftp.retrbinary("RETR " + fileName, outFile.write)
                    except:
                        self.logger.error("Error retrieving files from NCBI")
                        raise

    def _getStrainNames(self):
        """ """
        ftp = FTP(self.ftpurl)
        ftp.login()
        ftp.cwd(self.folder)
        straindirectories = []
        self.toretrieveg = []
        self.toretrieves = []
        ftp.retrlines("NLST " , straindirectories.append)
        self.logger.info('--> SCANNING DIRECTORIES AT NCBI...')
        for strainName in straindirectories:
            for genus in self.genuslist:
                match = re.search(r'^%s' % genus, strainName)
                if match:
                    self.logger.info('FOUND ' + genus + ' AS ' + strainName)
                    self.toretrieveg.append(strainName)
            for species in self.specieslist:
                match = re.search(r'^%s' % species, strainName)
                if match:
                    self.logger.info('FOUND ' + species + ' AS ' + strainName)
                    self.toretrieves.append(strainName)

    def _getTargetNames(self):
        """ """
        M = MetaphlanOut(self.metaphlanfile, self.abundance)
        self.genuslist = M.genuses
        self.specieslist = M.species
        self.logger.info('--> GETTING GENERA FROM METAPHLAN')
        for g in self.genuslist:
            self.logger.info(' ' + g)
        self.logger.info('--> GETTING SPECIES FROM METAPHLAN')
        for s in self.specieslist:
            self.logger.info(' ' + s)


LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']

def __main__():
    """ main function """
    parser = optparse.OptionParser()
    parser.add_option('--metaph', dest='metaphlanfile', help='MetaPhlAn output file (tabular)')
    parser.add_option('--abund', dest='abundance', type="float", help='Abundance threshold as percentage')
    parser.add_option('--faag', dest='faagfile', help='Output FASTA file for genus proteome')
    parser.add_option('--faas', dest='faasfile', help='Output FASTA file for species proteome')
    parser.add_option('--log', dest='logfile', help='Log file')
    parser.add_option('--loglevel', dest='loglevel', choices=LOG_LEVELS, help='Logging level (default: INFO)', default='INFO')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    log_level = getattr(logging, options.loglevel)
    kwargs = {'format' : LOG_FORMAT,
              'datefmt' : LOG_DATEFMT,
              'level' : log_level}
    if options.logfile:
        kwargs['filename'] = options.logfile
    logging.basicConfig(**kwargs)
    logger = logging.getLogger('Get proteomes from MetaPhlAn')

    S = GetData(options, logger)
    S.getData()


if __name__ == "__main__":
    __main__()
