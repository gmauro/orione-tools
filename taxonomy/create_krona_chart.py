#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Build a Krona pie chart from different formats
"""

import optparse
import subprocess
import sys

def __main__():
    parser = optparse.OptionParser()
    parser.add_option('--input-type', dest='inputtype', choices=['metaphlan', 'taxonomy'], help='Source of input file')
    parser.add_option('--input', dest="input", help="Input file")
    parser.add_option('--output', dest="output", help="Output file")
    parser.add_option('--min', dest='min', type='int', default=0, help='Keep only taxa with value greater than min (default 0)')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # Convert file to tabular format
    if options.inputtype == 'metaphlan':
        cmd = 'metaphlan2krona.py -p %s -k krona.txt' % options.input
        print('Converting from MetaPhlan')
        print('Converting in tabular format')
        print("Command to be executed: %s" % cmd)
        subprocess.check_call(cmd, shell=True)
    elif options.inputtype == 'taxonomy':
        print('Converting from taxonomy')
        print('Counting duplicate rows')
        counts = dict()
        with open(options.input, 'r') as z:
            for line in z.readlines():
                cols = line.split('\t')
                to_remove = (0, 1, 2, 4, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 19, 21, 23, 24)
                cols[:] = [item for i, item in enumerate(cols) if i not in to_remove]
                key = "\t".join(cols)
                counts[key] = counts.get(key, 0) + 1
        # Write to file
        with open('krona.txt','w') as f:
            for k, v in counts.items():
                if v > options.min:
                    f.write(''.join("%s\t%s" % (v, k)) + '\n')
    else:
        parser.error("Input type %s not allowed" % options.inputtype)
    
    # Convert tabular file to Krona chart
    cmd = 'ktImportText krona.txt -o %s' % options.output
    print('Creating Krona chart')
    print("Command to be executed: %s" % cmd)
    sys.stdout.flush()
    subprocess.check_call(cmd, shell=True)


if __name__ == "__main__":
    __main__()
