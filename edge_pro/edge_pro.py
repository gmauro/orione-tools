# -*- coding: utf-8 -*-
"""
Wrapper for EDGE - Gene expression in Prokaryotes
Author: Paolo Uva paolo dot uva at crs4 dot it
Date: March 18, 2013
"""

import glob
import optparse
import os
import shutil
import subprocess
import tempfile
import sys

def __main__():
    # Parse Command Line
    parser = optparse.OptionParser()
    # Input
    parser.add_option('-g', '--genome', dest="genome")
    parser.add_option('-p', '--ptt', dest="ptt")
    parser.add_option('-r', '--rnt', dest="rnt")
    parser.add_option('-u', '--input1', dest="input1")
    parser.add_option('-v', '--input2', dest="input2")
    parser.add_option('-t', '--num-threads', dest="num_threads", type='int')
    parser.add_option('-m', '--minInsertSize', dest="minInsertSize", type='int')
    parser.add_option('-M', '--maxInsertSize', dest="maxInsertSize", type='int')
    parser.add_option('-w', '--window', dest="window", type='int')
    parser.add_option('-i', '--utrSize', dest="utrSize", type='int')
    parser.add_option('-x', '--similarity', dest="similarity", type='float')
    parser.add_option('-c', '--minCoverage', dest="minCoverage", type='int')
    parser.add_option('-l', '--readLength', dest="readLength", type='int')
    # Output
    parser.add_option('--out-aln', dest="out_aln")
    parser.add_option('--out-rpkm', dest="out_rpkm")
    parser.add_option('--out-log', dest="out_log")
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # Build command
    if options.input2:
        input2_flag = '-v %s' % (options.input2)
    else:
        input2_flag = ''
    if options.num_threads is not None:
        num_threads_flag = '-t %d' % options.num_threads
    else:
        num_threads_flag = ''
    if options.minInsertSize is not None:
        minInsertSize_flag = '-m %d' % options.minInsertSize
    else:
        minInsertSize_flag = ''
    if options.maxInsertSize is not None:
        maxInsertSize_flag = '-M %d' % options.maxInsertSize
    else:
        maxInsertSize_flag = ''
    if options.window is not None:
        window_flag = '-w %d' % options.window
    else:
        window_flag = ''
    if options.utrSize is not None:
        utrSize_flag = '-i %d' % options.utrSize
    else:
        utrSize_flag = ''
    if options.similarity is not None:
        similarity_flag = '-x %s' % options.similarity
    else:
        similarity_flag = ''
    if options.readLength is not None:
        readLength_flag = '-l %d' % (options.readLength)
    else:
        readLength_flag = ''
    if options.minCoverage is not None:
        minCoverage_flag = '-c %d' % options.minCoverage
    else:
        minCoverage_flag = ''
    
    wd = tempfile.mkdtemp()
    try:
        prefix = os.path.join(wd, 'out')
        cl = 'edge.pl -g %s -p %s -r %s -u %s -o %s %s %s %s %s %s %s %s %s %s' % (options.genome, options.ptt, options.rnt, options.input1, prefix, input2_flag, num_threads_flag, minInsertSize_flag, maxInsertSize_flag, window_flag, utrSize_flag, similarity_flag, readLength_flag, minCoverage_flag)
        print cl
        
        if options.out_log:
            sout = open(options.out_log, 'w')
        else:
            sout = sys.stdout
        try:
            subprocess.check_call(cl, stdout=sout, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because edge.pl calls bowtie2 and count which write some logging info there
        finally:
            if sout != sys.stdout:
                sout.close()

        # Move alignment file
        shutil.move(prefix + '.alignments', options.out_aln)
        # Concatenate multiple RPKM files together
        with open(options.out_rpkm, 'wb') as destination:
            for filename in glob.iglob(prefix + '.rpkm_*'):
                with open(filename, 'rb') as source:
                    shutil.copyfileobj(source, destination)
    finally:
        shutil.rmtree(wd)


if __name__ == "__main__":
    __main__()
