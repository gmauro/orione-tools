<tool id="snpsift_dbnsfp" name="SnpSift dbNSFP" version="1.0">
  <description>Annotate with dbNSFP</description>
  <requirements>
    <requirement type="package" version="3.3">snpEff</requirement>
  </requirements>
  <command>
    java -jar ${GALAXY_DATA_INDEX_DIR}/shared/jars/snpeff/3.3/SnpSift.jar dbnsfp
    -v ${GALAXY_DATA_INDEX_DIR}/shared/jars/snpeff/3.3/data/dbNSFP2.0.txt $inputFile 2> $logFile > $outputFile
  </command>
  <inputs>
    <param format="vcf" name="inputFile" type="data" label="Variant file (VCF)"/>
  </inputs>
  <outputs>
    <data format="vcf" name="outputFile" label="${tool.name} on ${on_string}: VCF" />
    <data format="txt" name="logFile" label="${tool.name} on ${on_string}: log" />
  </outputs>
  <help>
**What it does**

This tool adds annotations from dbNSFP2.0_, an integrated database of human functional predictions from multiple algorithms (SIFT, Polyphen2, LRT and MutationTaster, PhyloP and GERP++, etc.).

.. _dbNSFP2.0: http://snpeff.sourceforge.net/SnpSift.html#dbNSFP

Annotations::

  - Ensembl_transcriptid: Ensembl transcript ids (separated by ';')
  - Uniprot_acc: Uniprot accession number. Multiple entries separated by ';'
  - Interpro_domain: Interpro domain or conserved site on which the variant locates
  - SIFT_score: if a score is smaller than 0.05, is predicted as 'D(amaging)',  otherwise as 'T(olerated)'
  - Polyphen2_HVAR_pred: prediction based on HumVar, 'D' ('probably damaging'), 'P' ('possibly damaging') and 'B' ('benign')
  - GERP++_NR: GERP++ neutral rate
  - GERP++_RS: GERP++ RS score, the larger the score, the more conserved the site
  - 29way_logOdds: SiPhy score based on 29 mammals genomes. The larger the score, the more conserved the site
  - 1000Gp1_AF: alternative allele frequency in the whole 1000Gp1 data
  - 1000Gp1_AFR_AF: alternative allele frequency in the 1000Gp1 African descendent samples
  - 1000Gp1_EUR_AF: alternative allele frequency in the 1000Gp1 European descendent samples
  - 1000Gp1_AMR_AF: alternative allele frequency in the 1000Gp1 American descendent samples
  - 1000Gp1_ASN_AF: alternative allele frequency in the 1000Gp1 Asian descendent samples
  - ESP6500_AA_AF: alternative allele frequency in the Afrian American samples of the NHLBI GO Exome Sequencing Project (ESP6500 data set)
  - ESP6500_EA_AF: alternative allele frequency in the European American samples of the NHLBI GO Exome Sequencing Project (ESP6500 data set)

------

**License and citation**

This Galaxy tool is Copyright © 2013 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

If you use this tool in Galaxy, please cite the website http://orione.crs4.it/

This tool uses `SnpSift`_, which is licensed separately. Please cite |Cingolani2012|_.

.. _SnpSift: http://snpeff.sourceforge.net/SnpSift.html
.. |Cingolani2012| replace:: Cingolani, P., *et al.* (2012) Using *Drosophila melanogaster* as a model for genotoxic chemical mutational studies with a new program, SnpSift. *Front. Genet.*, 3:35
.. _Cingolani2012: http://www.frontiersin.org/Toxicogenomics/10.3389/fgene.2012.00035/abstract
  </help>
</tool>
