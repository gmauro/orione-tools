# -*- coding: utf-8 -*-
"""
Glimmer3 --> anomaly
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Anomaly input options...'
    parser = optparse.OptionParser()
    parser.add_option('--anSequence', dest='sequence', help='')
    parser.add_option('--anCoords', dest='coords', help='')
    parser.add_option('--anCheckFirstCodon', action='store_true', dest='check_first_codon', help='')
    parser.add_option('--anCheckStopCodon', action='store_true', dest='check_stop_codon', help='')
    parser.add_option('--anStartCodons', dest='start_codons', help='')
    parser.add_option('--anStopCodons', dest='stop_codons', help='')
    parser.add_option('--anOutput', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # build Anomaly command to be executed
    # sequence file
    sequence = options.sequence
    coords = options.coords
    if options.start_codons:
        start_codons = '-A %s' % (options.start_codons)
    else:
        start_codons = ''
    if options.stop_codons:
        stop_codons = '-Z %s' % (options.stop_codons)
    else:
        stop_codons = ''
    if options.check_first_codon:
        check_first_codon = '-s'
    else:
        check_first_codon = ''
    if options.check_stop_codon:
        check_stop_codon = '-t'
    else:
        check_stop_codon = ''
    output = options.output
    logfile = options.logfile
    
    # Build Anomaly command
    cmd = 'anomaly %s %s %s %s %s %s > %s' % (start_codons, check_first_codon, check_stop_codon, stop_codons, sequence, coords, output)
    print '\nAnomaly command to be executed: \n %s' % (cmd)
    
    print 'Executing Anomaly...'
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Anomaly executed!'


if __name__ == "__main__":
    __main__()
