# -*- coding: utf-8 -*-
"""
Glimmer3 --> long-orfs
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing Long-ORFs input options...'
    parser = optparse.OptionParser()
    parser.add_option('--loSequence', dest='sequence', help='')
    parser.add_option('--loStartCodons', dest='start_codons', help='')
    parser.add_option('--loEntropy', dest='entropy', help='')
    parser.add_option('--loFixed', action='store_true', dest='fixed', help='')
    parser.add_option('--loMinLen', dest='min_len', type='int', help='')
    parser.add_option('--loIgnore', dest='ignore', help='')
    parser.add_option('--loLinear', action='store_true', dest='linear', help='')
    parser.add_option('--loLengthOpt', action='store_true', dest='length_opt', help='')
    parser.add_option('--loNoHeader', action='store_true', dest='no_header', help='')
    parser.add_option('--loMaxOverlap', dest='max_olap', type='int', help='')
    parser.add_option('--loCutoff', dest='cutoff', type='float', help='')
    parser.add_option('--loWithoutStops', action='store_true', dest='without_stops', help='')
    parser.add_option('--loTransTable', dest='trans_table', type='int', help='')
    parser.add_option('--loStopCodons', dest='stop_codons', help='')
    parser.add_option('--loOutput', dest='output', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # build Long-ORFs command to be executed
    sequence = options.sequence
    if options.start_codons:
        start_codons = '--start_codons %s' % (options.start_codons)
    else:
        start_codons = ''
    if options.entropy:
        entropy = '--entropy %s' % (options.entropy)
    else:
        entropy = ''
    if options.fixed:
        fixed = '--fixed'
    else:
        fixed = ''
    if options.min_len is not None:
        min_len = '--min_len %d' % (options.min_len)
    else:
        min_len = ''
    if options.ignore:
        ignore = '--ignore %s' % (options.ignore)
    else:
        ignore = ''
    if options.linear:
        linear = '--linear'
    else:
        linear = ''
    if options.length_opt:
        length_opt = '--length_opt'
    else:
        length_opt = ''
    if options.no_header:
        no_header = '--no_header'
    else:
        no_header = ''
    if options.max_olap is not None:
        max_olap = '--max_olap %d' % (options.max_olap)
    else:
        max_olap = ''
    if options.cutoff is not None:
        cutoff = '--cutoff %s' % (options.cutoff)
    else:
        cutoff = ''
    if options.without_stops:
        without_stops = '--without_stops'
    else:
        without_stops = ''
    if options.trans_table is not None:
        trans_table = '--trans_table %s' % (options.trans_table)
    else:
        trans_table = ''
    if options.stop_codons:
        stop_codons = '--stop_codons %s' % (options.stop_codons)
    else:
        stop_codons = ''
    output = options.output
    logfile = options.logfile
    
    # Build Long-ORFs command
    cmd = 'long-orfs %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (sequence, start_codons, entropy, fixed, min_len, ignore, linear, length_opt, no_header, max_olap, cutoff, without_stops, trans_table, stop_codons, output)
    print '\nLong-ORFs command to be executed: \n %s' % (cmd)
    
    print 'Executing Long-ORFs...'
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Long-ORFs executed!'


if __name__ == "__main__":
    __main__()
