# -*- coding: utf-8 -*-
"""
Glimmer3 --> glimmer3
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing input options...'
    parser = optparse.OptionParser()
    parser.add_option('--glSequence', dest='sequence', help='')
    parser.add_option('--glIcm', dest='icm', help='')
    #parser.add_option('--glPrefix', dest='prefix', help='')
    parser.add_option('--glStartCodons', dest='start_codons', help='')
    parser.add_option('--glRbsPwm', dest='rbs_pwm', help='')
    parser.add_option('--glGcPercent', dest='gc_percent', type='float', help='')
    parser.add_option('--glEntropy', dest='entropy', help='')
    parser.add_option('--glFirstCodon', action='store_true', dest='first_codon', help='')
    parser.add_option('--glGeneLen', dest='gene_len', type='int', help='')
    parser.add_option('--glIgnore', dest='ignore', help='')
    parser.add_option('--glLinear', action='store_true', dest='linear', help='')
    parser.add_option('--glOrfCoords', dest='orf_coords', help='')
    parser.add_option('--glSeparateGenes', action='store_true', dest='separate_genes', help='')
    parser.add_option('--glMaxOverlap', dest='max_olap', type='int', help='')
    parser.add_option('--glStartProbs', dest='start_probs', help='')
    parser.add_option('--glIgnoreScoreLen', dest='ignore_score_len', type='int', help='')
    parser.add_option('--glNoIndep', action='store_true', dest='no_indep', help='')
    parser.add_option('--glThreshold', dest='threshold', type='int', help='')
    parser.add_option('--glExtend', action='store_true', dest='extend', help='')
    parser.add_option('--glTransTable', dest='trans_table', type='int', help='')
    parser.add_option('--glStopCodons', dest='stop_codons', help='')
    parser.add_option('--glDetail', dest='detail', help='')
    parser.add_option('--glPredict', dest='predict', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')
    
    # build GLIMMER command to be executed
    # sequence file
    sequence = options.sequence
    # icm file
    icm = options.icm
    # prefix (not needed)
    prefix = 'prefix'
    # start codons
    if options.start_codons:
        start_codons = '--start_codons %s' % (options.start_codons)
    else:
        start_codons = ''
    # rbs_pwm
    if options.rbs_pwm:
        rbs_pwm = '--rbs_pwm %s' % (options.rbs_pwm)
    else:
        rbs_pwm = ''
    # gc percentage
    if options.gc_percent is not None:
        gc_percent = '--gc_percent %s' % (options.gc_percent)
    else:
        gc_percent = ''
    # entropy
    if options.entropy:
        entropy = "--entropy '%s'" % (options.entropy)
    else:
        entropy = ''
    # first_codon
    if options.first_codon:
        first_codon = '--first_codon'
    else:
        first_codon = ''
    # gene length
    if options.gene_len is not None:
        gene_len = '--gene_len %d' % (options.gene_len)
    else:
        gene_len = ''
    # ignore
    if options.ignore:
        ignore = '--ignore %s' % (options.ignore)
    else:
        ignore = ''
    # linear
    if options.linear:
        linear = '--linear'
    else:
        linear = ''
    # orf_coords
    if options.orf_coords:
        orf_coords = '--orf_coords %s' % (options.orf_coords)
    else:
        orf_coords = ''
    # separate genes
    if options.separate_genes:
        separate_genes = '--separate_genes'
    else:
        separate_genes = ''
    # max overlap
    if options.max_olap is not None:
        max_olap = '--max_olap %d' % (options.max_olap)
    else:
        max_olap = ''
    # start probs
    if options.start_probs:
        start_probs = '--start_probs %s' % (options.start_probs)
    else:
        start_probs = ''
    # ignore score length
    if options.ignore_score_len is not None:
        ignore_score_len = '--ignore_score_len %d' % (options.ignore_score_len)
    else:
        ignore_score_len = ''
    # no indep
    if options.no_indep:
        no_indep = '--no_indep'
    else:
        no_indep = ''
    # threshold
    if options.threshold is not None:
        threshold = '--threshold %d' % (options.threshold)
    else:
        threshold = ''
    # extend
    if options.extend:
        extend = '--extend'
    else:
        extend = ''
    # trans table
    if options.trans_table is not None:
        trans_table = '--trans_table %d' % (options.trans_table)
    else:
        trans_table = ''
    # stop codons
    if options.stop_codons:
        stop_codons = '--stop_codons %s' % (options.stop_codons)
    else:
        stop_codons = ''
    logfile = options.logfile
    
    # Build Glimmer command
    cmd = 'glimmer3 %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (start_codons, rbs_pwm, gc_percent, entropy, first_codon, gene_len, ignore, linear, orf_coords, separate_genes, max_olap, start_probs, ignore_score_len, no_indep, threshold, extend, trans_table, stop_codons, sequence, icm, prefix)
    print '\nGlimmer command to be executed: \n %s' % (cmd)
    
    print 'Executing Glimmer...'
    if logfile:
        log = open(logfile, 'w')
    else:
        log = sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Glimmer executed!'
    
    shutil.move(prefix + ".detail", options.detail)
    shutil.move(prefix + ".predict", options.predict)


if __name__ == "__main__":
    __main__()
